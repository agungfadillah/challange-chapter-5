const express = require('express')
const router = express.Router()



//route level middleware
router.use((request, response, next) => {
    console.log('router level middleware')
    next()
})
router.get('/login', (request, response) => {
    response.render('login')
})
router.post('/login', (request, response) => {

    const {email, password} = request.body

    users.push({email, password})

    response.redirect('/game')
})
router.get('/register', (request, response) => {
    response.render('register')
})
router.post('/register', (request, response) => {
    const {email, password} = request.body

    users.push({email, password})

    response.redirect('/')
})
router.get('/home', (request, response) => {
    response.render('home')
})
router.post('/home', (request, response) => {
    response.redirect('/')
})

router.get('/game', (request, response) => {
    response.render('game')
})
router.post('/game', (request, response) => {
    response.redirect('/')
})


module.exports = router