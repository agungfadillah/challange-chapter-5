const express = require('express')
const app = express()
const port = 3000

app.use(express.urlencoded({extended: false}))

app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/images', express.static(__dirname + 'public/images'))
app.use('/assets', express.static(__dirname + 'public/assets'))
app.use('/javascript', express.static(__dirname + 'public/javascript'))


app.set('views', './views')
app.set('view engine', 'ejs')

app.get("/home", (request, response) => {
    response.render('home')
})

app.get("/game", (request, response) => {
    response.render('game')
})

app.get("/register", (request, response) => {
    response.render('register')
})

app.get("/login", (request, response) => {
    response.render('login')
})

app.use(express.json())
app.use((request, response, next ) => {
    console.log(`${request.method} ${request.url}`)
    next()
})

const authRouter = require("./routes/auth.js")
app.use("/auth", authRouter)


app.use((err, req, res, next) => {
    res.status(500).json({
        status: ' fail',
        errors: err.message
    })
})

app.use((req, res, next) => {
    res.status(404).json({
        status: 'fail',
        error: 'Are you lost?'
    })
})

app.listen(port, () => {
    console.log(`Web started at port : ${port}`)
})